minish (MINImal SHell) is a minimal command-line interpreter.

Canonical url: “https://repo.or.cz/minish.git”.

minish is released under the terms of the GNU AGPL version 3:
see “LICENSE.txt” or “https://gnu.org/licenses/agpl.html”.

Goals:
• minimalism: minish shall have few features and rely on commands;
• no built-ins: if it looks like a command, it is an external command;
• modularity: a user who does not like something can change just that;
• extensibility: adding features requires little to no modifications;
• usability: minish shall not be a (non-)Turing tar-pit.

Non-goals (good things that stand in the way of goals):
• being a POSIX shell;
• efficiency.

minish is developed in C11 using the C POSIX library.

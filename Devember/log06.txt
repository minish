Suppose that, using bash,
I launch a minish script, press Ctrl+Z, and run “fg”.
I would expect the script to resume executing.
In order to do that,
minish and its command must be in the same process group,
that bash calls “job”.

In script mode,
each command is executed in the same process group as the shell;
in interactive mode,
each command is executed in its own process group.
This has been standard practice for a long time,
and I think it is very sensible.

signal name	keys to press	default action
-----------------------------------------------------------------
SIGINT		CTRL+C		terminate
SIGQUIT		CTRL+\		terminate and produce a core dump
SIGTSTP		CTRL+Z		suspend

SIGINT, SIGQUIT, and SIGTSTP can be produced interactively
and are sent to the foreground process group.
In interactive mode:
if a command is executing, it receives signals,
but minish does not, because they are in different process groups;
if no commands are executing, minish reacts to signals normally.
In script mode,
both minish and the command it is executing are sent signals,
but minish ignores them;
if the command stops executing because of SIGINT or SIGTSTP,
minish does, too, by raising the same signal without ignoring it;
if the command stops executing becouse of SIGQUIT,
minish terminates by returning EXIT_FAILURE,
because the user probably did not want its core to be dumped.

Enough for today.

Suggested readings:
https://wiki.archlinux.org/index.php/Core_dump/
https://gnu.org/software/libc/manual/html_node/Termination-Signals.html

Today I have done a little refactoring and fixed a few bugs.
Command substitution works.
Each file is short, simple, and sensible.
I am happy with what I have done.

I forgot to commit the devlog yesterday.
I used “commit --amend” to add it to yesterday's commit,
which I have already pushed.
Then I checked out into the mob branch and try to merge that commit,
but I could not, because the amended commit was still in.
So I reset with “reset HEAD~1” and then I tried to merge,
but I still could not because I had not commited changes.
So I cheated.
I copied the files with uncommited changes in another directory,
checked out those files, and merged mob with master.
Then, I tried to push my changes and failed,
because origin still had the unamended commit.
So I cheated again, and used “push --force”.
The moral of the story is this:
do not try to rewrite history you have already pushed,
especially if you, like me, do not know what you are doing.

I spent a few days trying to make minish-eval work,
without adding any functionality to minish,
but I still think it was worth it.
It is not really for modularity:
if someone wants to change minish-eval,
they might as well implement their own shell.
The existence of minish-eval is itself a functionality.
If a program has a string and wants to evaluate it with minish,
it can just fork() and execv() into minish-eval,
passing the string to execv,
where before they had to also establish a pipe and writing into it.
That is why sh has the “-c” option.

Enough for today.

Suggested readings:
https://softwareengineering.stackexchange.com/questions/151541/
https://landley.net/notes-2014.html#23-04-2014

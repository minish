When minish is called without arguments, it reads from stdin.
This is interactive mode.
When it is called with an argument, it reads from the file thus named.
This is script mode.

In interactive mode,
minish should not terminate when Ctrl+C is pressed
and a command is executing.
There are two ways of doing this:
ignoring SIGINT,
or avoid being in the foreground process group so to not receive it.
Yesterday minish used the first way, but today I use the second.

When minish spawns a new process, with fork(),
this process puts itself in a new group just for itself
and declares this group as foreground.
When the child process terminates,
minish declare its own process group as foreground.

tcsetpgrp(fd, gpid) puts the gpid process group in the foreground
of the fd terminal.
If gpid is in the background, it receives the signal SIGTTOU;
it means “stay put: you can do any output right now”
and its default action is to suspend, but it can be ignored.
So, the child must:
1. create its own process group;
2. disable the handling of SIGTTOU;
3. puts its process group in the foreground;
4. re-enable the handling of SIGTTOU.

Using process groups paves the way to job control.
Without process groups, the shell could ignore SIGINT anyway,
but other background processes cannot:
if they did, we would be unable to stop them.

Enough for today.

Suggested readings:
https://vidarholen.net/contents/blog/?p=34
https://gnu.org/software/bash/manual/html_node/Job-Control-Basics.html

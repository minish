I need an IPC mechanism that can be used after execv(),
but cannot be used anymore after I dispose of it.

IPC mechanism		after execv()?		can renounce?
signal				✓
pipe				✓			✓
FIFO				✓
message queue						✓
semaphore						✓
shared memory						✓
file				✓
socket				✓

A pipe is the best choice.
An open file descriptor remains open through execv(),
but can be closed at will.

An open file descriptor remains open through execv(),
but, in order to use it, the program must know it.
There is no way in POSIX to get a list of open file descriptors;
on GNU/Linux, this is usually done by looking into /proc/self/fd/.
I pass the bytes of the file descriptor as arguments to minish-once,
one byte per argument.
If the byte is not 0, it is followed by ␀.
This is not elegant, but it works and it is portable.

Before terminating, minish-once writes its return code in the pipe;
minish reads it and terminates itself with the same error code.
minish-once closes its end of the pipe before attempting execv(),
so that the executing command has no way to access it.
If execv() fails, minish-once does not write in the pipe,
because minish should not terminate in that case.

minish uses fcntl() to set the O_NONBLOCK flag on the pipe,
so that read() returns -1 instead of blocking when the pipe is empty.

Enough for today.

Suggested reading:
https://beej.us/guide/bgipc/output/html/singlepage/bgipc.html

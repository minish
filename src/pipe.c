#define _POSIX_C_SOURCE 200809L
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

#define noreturn _Noreturn void

static char const* argv0;

static noreturn pexit(char const* const message) {
	int const error = errno;
	fprintf(stderr, "%s: ", argv0);
	errno = error;
	perror(message);
	exit(EXIT_FAILURE);
}

static noreturn exec(char* const command) {
	char* const argv[] = { "minish-eval", command, NULL };
	execv(argv[0], argv);
	pexit(argv[0]);
}

static pid_t try_fork(void) {
	pid_t const pid = fork();
	if (pid == -1) {
		pexit("fork()");
	}
	return pid;
}

static void move_fd(int const old, int const new) {
	if (dup2(old, new) == -1) {
		pexit("dup2()");
	}
	close(old);
}

int main(int argc, char** argv) {
	if (argc > 1) {
		argv0 = argv[0];
		if (argc == 2) {
			exec(argv[1]);
		}
		pid_t* const pids = malloc(sizeof(argc - 1));
		if (!pids) {
			pexit("malloc()");
		}
		for (int i = 0; i < argc - 2; ++i) {
			int fds[2];
			if (pipe(fds) == -1) {
				pexit("pipe()");
			}
			pids[i] = try_fork();
			if (!pids[i]) {
				close(fds[0]);
				move_fd(fds[1], 1);
				exec(argv[i + 1]);
			}
			close(fds[1]);
			move_fd(fds[0], 0);
		}
		pids[argc - 2] = try_fork();
		if (!pids[argc - 2]) {
			exec(argv[argc - 1]);
		}
		int ret = 0;
		for (int i = 0; i < argc - 1; ++i) {
			int s;
			if (waitpid(pids[i], &s, 0) == -1) {
				pexit("waitpid()");
			}
			if (!ret && WIFEXITED(s) && WEXITSTATUS(s)) {
				ret = s;
			}
		}
		return ret;
	}
}

#ifndef WORDS_H
#define WORDS_H

#define _POSIX_C_SOURCE 200809L
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include "try.h"

typedef struct {
	char* word;
	size_t len;
	bool eof;
} ReadWord;

typedef struct {
	size_t size;
	size_t len;
	char** words;
} Words;

static char** alloc(size_t const size) {
	return try_malloc(size * sizeof(char*));
}

static bool adjust(size_t const len, size_t* const size) {
	if (len >= *size) {
		out_of_memory_if(*size == SIZE_MAX);
		if (*size > SIZE_MAX / 2u) {
			*size = SIZE_MAX;
		} else {
			*size *= 2u;
		}
		return true;
	}
	return false;
}

static Words new() {
	Words const words = {
		.size = 2u * sizeof(*words.words),
		.len = 0u,
		.words = alloc(2u),
	};
	return words;
}

static void push(Words words[static const 1], char* const s) {
	if (adjust(words->len * sizeof(*words->words), &words->size)) {
		words->words = try_realloc(words->words, words->size);
	}
	words->words[words->len++] = s;
}

static bool read_char(int const fd, char* const c) {
	ssize_t const len = read(fd, c, 1u);
	if (len == -1) {
		pexit("read()");
	}
	return len;
}

static ReadWord read_word(int const fd) {
	ReadWord w;
	w.len = 0u;
	char c;
	w.eof = !read_char(fd, &c);
	if (w.eof) {
		w.word = NULL;
	} else {
		size_t size = 1u;
		w.word = try_malloc(size);
		w.word[0] = c;
		while (c) {
			if (!read_char(fd, &c)) {
				c = '\0';
				w.eof = true;
			}
			if (adjust(++w.len, &size)) {
				w.word = try_realloc(w.word, size);
			}
			w.word[w.len] = c;
		}
	}
	return w;
}

#endif

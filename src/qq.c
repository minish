#include <stdio.h>

int main(int argc, char** argv) {
	if (argc > 1) {
		fputs(argv[1], stdout);
		for (int i = 2; i < argc; ++i) {
			putchar('\0');
			fputs(argv[i], stdout);
		}
	}
}

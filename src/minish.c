#define _POSIX_C_SOURCE 200809L
#include <fcntl.h>
#include <locale.h>
#include <signal.h>
#include <stdbool.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include "ipc.h"
#include "try.h"
#include "words.h"

static int try_open(char const name[static const 1]) {
	int const fd = open(name, O_RDONLY);
	if (fd == -1) {
		pexit(name);
	}
	return fd;
}

static void try_setpgid(pid_t const pid, pid_t const pgid) {
	if (setpgid(pid, pgid) == -1) {
		pexit("setpgid()");
	}
}

static int wait_and_check(pid_t const pid, bool const interactive) {
	return interactive ? try_waitpid(pid, 0) : wait_script(pid);
}

int main(int argc, char** argv) {
	setlocale(LC_ALL, "");
	argv0 = argv[0] ? argv[0] : "minish";
	bool const interactive = argc <= 1;
	if (!interactive) {
		accept_interactive_signals(false);
	}
	int const script = interactive ? 0 : try_open(argv[1]);
	char* const read_argv[] = {
		"minish-read",
		interactive ? "stdin" : argv[1],
		NULL,
	};
	int status;
	do {
		Pipe const pipe = make_pipe();
		pid_t const eval_pid = try_fork();
		if (!eval_pid) {
			if (interactive) {
				accept_interactive_signals(false);
			} else {
				close(script);
			}
			close(pipe.write);
			Words words = new();
			push(&words, "minish-eval");
			ReadWord word;
			do {
				word = read_word(pipe.read);
				push(&words, word.word);
			} while (word.word);
			close(pipe.read);
			try_execv(words.words);
		}
		close(pipe.read);
		if (interactive) {
			try_setpgid(eval_pid, 0);
			int const tty = try_open("/dev/tty");
			accept_signal(SIGTTOU, false);
			if(tcsetpgrp(tty, eval_pid) == -1) {
				pexit("tcsetpgrp()");
			}
			accept_signal(SIGTTOU, true);
			close(tty);
		}
		pid_t const read_pid = try_fork();
		if (!read_pid) {
			if (interactive) {
				try_setpgid(0, eval_pid);
			} else {
				accept_interactive_signals(true);
				move_fd(script, 0);
			}
			move_fd(pipe.write, 1);
			try_execv(read_argv);
		}
		close(pipe.write);
		status = wait_and_check(read_pid, interactive);
		if (WIFEXITED(status) && WEXITSTATUS(status) == 1) {
			kill(eval_pid, SIGHUP);
			return EXIT_FAILURE;
		}
		wait_and_check(eval_pid, interactive);
	} while (!(WIFEXITED(status) && WEXITSTATUS(status)));
}

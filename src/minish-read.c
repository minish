#define _POSIX_C_SOURCE 200809L
#include <errno.h>
#include <locale.h>
#include <stdio.h>

int main(int argc, char** argv) {
	setlocale(LC_ALL, "");
	char const* const name = argv[0] ? argv[0] : "minish-read";
	char const* const file = argc ? argv[1] : NULL;
	for (;;) {
		int const c = getchar();
		switch (c) {
		case EOF:
			if (ferror(stdin)) {
				int const tmp = errno;
				fprintf(stderr, "%s: ", name);
				errno = tmp;
				perror(file);
				return 1;
			}
			return 2;
		case '\n': return 0;
		}
		putchar(c);
	}
}

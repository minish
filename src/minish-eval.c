#define _POSIX_C_SOURCE 200809L
#include <locale.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "ipc.h"
#include "try.h"
#include "words.h"

static Words words;
static size_t size_l;

static void append(char* const s, size_t const len_r) {
	if (len_r) {
		char** const w = words.words + (words.len - 1u);
		bool const re = size_l;
		size_t const ll = re ? size_l - 1u : strcspn(*w, "()");
		out_of_memory_if(SIZE_MAX - ll < len_r + 1u);
		size_l = ll + len_r + 1u;
		if (re) {
			*w = try_realloc(*w, size_l);
		} else {
			char* const p = try_malloc(size_l);
			memcpy(p, *w, ll);
			*w = p;
		}
		memcpy(*w + ll, s, len_r);
		(*w)[size_l - 1u] = '\0';
	}
}

int main(int argc, char** argv) {
	setlocale(LC_ALL, "");
	if (argc <= 1) {
		return 0;
	}
	argv0 = argv[0];
	words = new();
	size_t level = 0u;
	size_t open_i = 0u;
	char* open_s = NULL;
	for (size_t i = 1u; argv[i]; ++i) {
		if (!level) {
			push(&words, argv[i]);
			size_l = 0u;
		}
		for (char* s = argv[i]; (s = strpbrk(s, "()")); ) {
			bool const open = *s == '(';
			*s++ = '\0';
			if (open && level != SIZE_MAX && !level++) {
				open_i = i;
				open_s = s;
			} else if (!open && level && !--level) {
				Pipe const p = make_pipe();
				pid_t const pid = try_fork();
				if (!pid) {
					close(p.read);
					move_fd(p.write, 1);
					i -= open_i;
					char** const v = alloc(i + 3u);
					v[0] = argv[0];
					v[1] = open_s;
					v[i + 2u] = NULL;
					argv += open_i + 1u;
					i *= sizeof(*v);
					memcpy(v + 2, argv, i);
					try_execv(v);
				}
				close(p.write);
				ReadWord w = read_word(p.read);
				append(w.word, w.len);
				free(w.word);
				while (!w.eof) {
					w = read_word(p.read);
					if (!w.word) {
						w.word = "";
					}
					push(&words, w.word);
				}
				close(p.read);
				wait_script(pid);
				append(s, strcspn(s, "()"));
			}
		}
	}
	push(&words, NULL);
	accept_interactive_signals(true);
	try_execv(words.words);
}

#ifndef IPC_H
#define IPC_H

#define _POSIX_C_SOURCE 200809L
#include <signal.h>
#include <stdbool.h>
#include <sys/wait.h>
#include <unistd.h>
#include "try.h"

typedef struct {
	int read;
	int write;
} Pipe;

static void accept_signal(int const signum, bool const accept) {
	if (signal(signum, accept ? SIG_DFL : SIG_IGN) == SIG_ERR) {
		pexit("signal()");
	}
}

static void accept_interactive_signals(bool const accept) {
	accept_signal(SIGINT, accept);
	accept_signal(SIGQUIT, accept);
	accept_signal(SIGTSTP, accept);
}

static Pipe make_pipe() {
	Pipe p;
	int fds[2];
	if (pipe(fds) == -1) {
		pexit("pipe()");
	}
	p.read = fds[0];
	p.write = fds[1];
	return p;
}

static void move_fd(int const old, int const new) {
	if (dup2(old, new) == -1) {
		pexit("dup2()");
	}
	close(old);
}

static int wait_script(pid_t const pid) {
	int status = try_waitpid(pid, WUNTRACED);
	while (WIFSTOPPED(status)) {
		if (WSTOPSIG(status) == SIGTSTP) {
			accept_signal(SIGTSTP, true);
			raise(SIGTSTP);
			accept_signal(SIGTSTP, false);
		}
		status = try_waitpid(pid, WUNTRACED);
	}
	if (WIFSIGNALED(status)) {
		switch (WTERMSIG(status)) {
		case SIGINT:
			accept_signal(SIGINT, true);
			raise(SIGINT);
		case SIGQUIT: exit(EXIT_FAILURE);
		}
	}
	return status;
}

#endif

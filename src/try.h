#ifndef TRY_H
#define TRY_H

#define _POSIX_C_SOURCE 200809L
#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

#define noreturn _Noreturn void

static char const* argv0;

static noreturn error(int const error, char const* const message) {
	fprintf(stderr, "%s: ", argv0);
	errno = error;
	perror(message);
	exit(EXIT_FAILURE);
}

static void out_of_memory_if(bool const cond) {
	if (cond) {
		error(ENOMEM, NULL);
	}
}

static noreturn pexit(char const* const message) {
	error(errno, message);
}

static noreturn try_execv(char* const argv[static const 2]) {
	execv(argv[0], argv);
	pexit(argv[0]);
}

static pid_t try_fork(void) {
	pid_t const pid = fork();
	if (pid == -1) {
		pexit("fork()");
	}
	return pid;
}

static void* try_malloc(size_t const size) {
	void* const p = malloc(size);
	if (!p) {
		pexit("malloc()");
	}
	return p;
}

static void* try_realloc(void* p, size_t const size) {
	p = realloc(p, size);
	if (!p) {
		pexit("realloc()");
	}
	return p;
}

static int try_waitpid(pid_t const pid, int const options) {
	int status;
	if (waitpid(pid, &status, options) == -1) {
		pexit("waitpid()");
	}
	return status;
}

#endif

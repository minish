#!/bin/sh

readonly CLANG="clang -DNDEBUG -ferror-limit=1 -flto \
-fvisibility=hidden -march=native -O3 -s -static -std=c11 -Werror \
-Weverything -Wno-disabled-macro-expansion -Wno-reserved-id-macro"

readonly NEWLINE='
'

readonly SED_SCRIPT='s_[ \t]*#include[ \t]*"\(.*\)".*_src/\1_p'

is_not_newer_than() {
	[ ! -f "$1" ] || [ -z "$(find "$1" -newer "$2")" ]
}

mkdir -p bin/
for SOURCE in src/*.c; do
	OBJECT="${SOURCE%.c}"
	OBJECT="bin${OBJECT#src}"
	IFS="$NEWLINE"
	for DEP in "$SOURCE" $(sed -n "$SED_SCRIPT" "$SOURCE"); do
		if is_not_newer_than "$OBJECT" "$DEP"; then
			if [ $# -eq 0 ]; then
				IFS=' '
				$CLANG -o "$OBJECT" "$SOURCE" &
			else
				"$@" -o "$OBJECT" "$SOURCE" &
			fi
			break
		fi
	done
done
ERROR=0
for JOB in $(jobs -p); do
	wait "$JOB" || ERROR=$?
done
exit $ERROR
